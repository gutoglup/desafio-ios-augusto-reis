//
//  RepositoriesTableViewController.swift
//  DesafioGitHub
//
//  Created by Augusto Reis on 30/12/16.
//  Copyright © 2016 Augusto Reis. All rights reserved.
//

import UIKit
import Kingfisher

class RepositoriesTableViewController: UITableViewController {

    var numberPage : Int = 0
    var isRefreshing : Bool = false
    
    var listOfRepositories = Array<Items>()
    var selectedRepository : Items?
    
    fileprivate let detailRepositorySegue = "detailRepositorySegue"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        pullRefreshNextRepositories()
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return listOfRepositories.count
    }


    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(forIndexPath: indexPath) as RepositoriesTableCell
        let repository = listOfRepositories[indexPath.row]
        
        cell.nameRepository.text = repository.name
        cell.descriptionRepository.text = repository.descriptionValue
        if let forkCount = repository.forksCount {
            cell.numberForks.text = "\(forkCount)"
        }
        if let numberLikes = repository.stargazersCount {
            cell.numberLikes.text = "\(numberLikes)"
        }
        cell.username.text = repository.owner?.login
        cell.fullNameUser.text = repository.fullName
        if  let imageString = repository.owner?.avatarUrl,
            let imageUrl = URL(string: imageString) {
            cell.imageUser.kf.setImage(with: imageUrl)
        }

        return cell
    }
    
    override func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.001
    }
 
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedRepository = listOfRepositories[indexPath.row]
        self.performSegue(withIdentifier: detailRepositorySegue, sender: self)
    }
    
    // MARK: - Scroll View delegate
    
    override func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView.contentOffset.y >= scrollView.contentSize.height - scrollView.frame.size.height {
            pullRefreshNextRepositories()
        }
    }

    // MARK: - Actions and Logic
    
    
    /// Método que atualiza os próximos repositórios
    func pullRefreshNextRepositories(){
        if !isRefreshing {
            isRefreshing = true
            numberPage += 1
            ApiService.listRepositories(page: numberPage, handler: { (items) in
                if let items = items {
                    self.listOfRepositories += items
                }
                self.tableView.reloadData()
                self.isRefreshing = false
            })
        }
    }
    

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if let pullRequestTableViewController = segue.destination as? PullRequestTableViewController{
            pullRequestTableViewController.selectedRepository = self.selectedRepository
        }
        
    }
 

}
