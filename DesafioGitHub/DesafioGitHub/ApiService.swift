//
//  ApiService.swift
//  DesafioGitHub
//
//  Created by Augusto Reis on 30/12/16.
//  Copyright © 2016 Augusto Reis. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireNetworkActivityIndicator
import SwiftyJSON

typealias CompletionHandler = (() -> ())?
typealias JsonItensHandler = (([Items]?) -> ())
typealias JsonRepositoryHandler = (( [PullRequest]?) -> ())

class ApiService {
    
    
    /// Serviço de requisição da lista de repositórios do GitHub
    ///
    /// - Parameters:
    ///   - page: número da página
    ///   - handler: closure de resposta
    static func listRepositories(page: Int, handler: JsonItensHandler?){
        guard let url = URL(string:LinkManager.listOfRepositories(page: page)) else {
            return
        }
        NetworkActivityIndicatorManager.shared.isEnabled = true
        Alamofire.request(url).validate().responseJSON { (dataResponse) in
            NetworkActivityIndicatorManager.shared.isEnabled = false
            switch(dataResponse.result){
            case .success(let value) :
                let repositoriesInfo = Repositories(object: value)
                if let handlerUnwarpped = handler {
                    handlerUnwarpped(repositoriesInfo.items)
                }
                
            case .failure(let error):
                print(error)
            }
        }
    }
    
    
    /// Serviço de requisição dos pulls request de um repositório específico
    ///
    /// - Parameters:
    ///   - user: login do usuário
    ///   - repository: nome do repositório
    ///   - handler: closure de resposta
    static func repositoryInformation(user:String, repository:String, handler: JsonRepositoryHandler?){
        guard let url = URL(string: LinkManager.listOfPullRequest(user: user, repository: repository)) else {
            return
        }
        
        NetworkActivityIndicatorManager.shared.isEnabled = true
        Alamofire.request(url).validate().responseJSON { (dataResponse) in
            NetworkActivityIndicatorManager.shared.isEnabled = false
            switch dataResponse.result {
            case .success(let value):
                var arrayRepository = [PullRequest]()
                if value is Array<Any> {
                    for repo  in value as! Array<Any> {
                        let pull = PullRequest(object: repo)
                        arrayRepository.append(pull)
                    }
                }
                if let handlerUnwarpped = handler {
                    handlerUnwarpped(arrayRepository)
                }
                
            case .failure(let error):
                print(error)
            }
            
            
        }
    }
    
}
