//
//  LinkManager.swift
//  DesafioGitHub
//
//  Created by Augusto Reis on 30/12/16.
//  Copyright © 2016 Augusto Reis. All rights reserved.
//

import Foundation

class LinkManager {
    
    static let pathFile = "Services"
    static let pathType = "plist"
    
    static let keyRepositories = "repositories"
    static let keyPullRequests = "pullRequests"
    
    static let tagPage = "<page>"
    static let tagUser = "<user>"
    static let tagRepository = "<repository>"
    
    
    /// Método que retorna o link da lista de repositórios
    ///
    /// - Parameter page: número da pagina
    /// - Returns: link da lista de repositórios
    static func listOfRepositories(page:Int)-> String {
        let contentFile = contentOfFile(path: pathFile, type: pathType)
        if var link = contentFile?[keyRepositories] as? String {
            link = link.replacingOccurrences(of: tagPage, with: "\(page)")
            return link
        }
        return ""
    }
    
    
    /// Método que retorna o link da lista de pull request
    ///
    /// - Parameters:
    ///   - user: login do usuário
    ///   - repository: nome do repositório
    /// - Returns: link da lista de pull request
    static func listOfPullRequest(user:String, repository:String) -> String {
        let contentFile = contentOfFile(path: pathFile, type: pathType)
        if var link = contentFile?[keyPullRequests] as? String {
            link = link.replacingOccurrences(of: tagUser, with: user)
            link = link.replacingOccurrences(of: tagRepository, with: repository)
            return link
        }
        return ""
    }
    
    
    /// Método que recupera os links de um arquivo
    ///
    /// - Parameters:
    ///   - path: nome do arquivo
    ///   - type: tipo do arquivo
    /// - Returns: conteúdo do arquivo
    static func contentOfFile(path: String, type: String)-> Dictionary<String,AnyObject>? {
        if let path = Bundle.main.path(forResource: path, ofType: type) {
            if let dictionary = NSDictionary(contentsOfFile: path) as? Dictionary<String,AnyObject> {
                return dictionary
            }
            
        }
        return nil
    }
    
}
