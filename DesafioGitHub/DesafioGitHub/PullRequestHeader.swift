//
//  PullRequestHeader.swift
//  DesafioGitHub
//
//  Created by Augusto Reis on 31/12/16.
//  Copyright © 2016 Augusto Reis. All rights reserved.
//

import UIKit

class PullRequestHeader: UIView {

    @IBOutlet weak var open: UILabel!
    @IBOutlet weak var closed: UILabel!
    
    @IBOutlet var containerView: UIView!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        xibSetup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        xibSetup()
    }
    
    func xibSetup(){
        let view = loadViewFromNib()
        view.frame = bounds
        view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        addSubview(view)
    }
    
    func loadViewFromNib() -> UIView{
        let bundle = Bundle(for: type(of:self))
        let nib = UINib(nibName: "PullRequestHeader", bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil)[0] as! UIView
        return view
    }

}
