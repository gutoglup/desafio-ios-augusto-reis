//
//  PullRequestTableCell.swift
//  DesafioGitHub
//
//  Created by Augusto Reis on 30/12/16.
//  Copyright © 2016 Augusto Reis. All rights reserved.
//

import UIKit

class PullRequestTableCell: UITableViewCell, ReusableView {

    @IBOutlet weak var titlePullRequest: UILabel!
    @IBOutlet weak var descriptionPullRequest: UILabel!
    @IBOutlet weak var imageUser: UIImageView!
    @IBOutlet weak var username: UILabel!
    @IBOutlet weak var datePull: UILabel!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
