//
//  RepositoriesTableCell.swift
//  DesafioGitHub
//
//  Created by Augusto Reis on 30/12/16.
//  Copyright © 2016 Augusto Reis. All rights reserved.
//

import UIKit

class RepositoriesTableCell: UITableViewCell, ReusableView {

    @IBOutlet weak var nameRepository: UILabel!
    @IBOutlet weak var descriptionRepository: UILabel!
    @IBOutlet weak var numberForks: UILabel!
    @IBOutlet weak var numberLikes: UILabel!
    @IBOutlet weak var imageUser: UIImageView!
    @IBOutlet weak var username: UILabel!
    @IBOutlet weak var fullNameUser: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
