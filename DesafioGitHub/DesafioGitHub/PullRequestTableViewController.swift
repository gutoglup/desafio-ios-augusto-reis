//
//  PullRequestTableViewController.swift
//  DesafioGitHub
//
//  Created by Augusto Reis on 30/12/16.
//  Copyright © 2016 Augusto Reis. All rights reserved.
//

import UIKit

class PullRequestTableViewController: UITableViewController {

    var selectedRepository : Items?
    var repositories :  [PullRequest]?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        if let login = selectedRepository?.owner?.login,
            let repositoryName = selectedRepository?.name {
            
            ApiService.repositoryInformation(user: login, repository: repositoryName, handler: { (repositories) in
                self.repositories = repositories
                self.tableView.reloadData()
            })
        }
        self.title = selectedRepository?.name
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        if repositories != nil {
            return 1
        }
        return 0
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let repositories = repositories {
            return repositories.count
        }
        
        return 0
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(forIndexPath: indexPath) as PullRequestTableCell
        let repository = self.repositories?[indexPath.row]
        cell.titlePullRequest.text = repository?.title
        cell.descriptionPullRequest.text = repository?.body
        cell.username.text = repository?.user?.login
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
        formatter.locale = Locale(identifier: "en_US_POSIX")
        if let datePull = formatter.date(from: (repository?.updatedAt)!) {
            formatter.dateFormat = "dd/MM/yyyy HH:mm"
            cell.datePull.text = formatter.string(from: datePull)
        }
        if  let imageString = repository?.user?.avatarUrl,
            let imageUrl = URL(string: imageString) {
            cell.imageUser.kf.setImage(with: imageUrl)
        }

        return cell
    }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let header = PullRequestHeader()
        header.open.text = "\(numberOfRepositories(state: .open)) opened"
        header.closed.text = "\(numberOfRepositories(state: .closed)) closed"
        return header
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 60
    }
    
    override func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.001
    }
 

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let repository = repositories?[indexPath.row]
        if  let htmlUrl = repository?.htmlUrl,
            let url = URL(string: htmlUrl) {
            if UIApplication.shared.canOpenURL(url){
                if #available(iOS 10.0, *) {
                    UIApplication.shared.open(url, options: [:], completionHandler: { (success) in
                        
                    })
                } else {
                    UIApplication.shared.openURL(url)
                }
            }
        }
    }
    
    // MARK: - Action and Logic
    
    func numberOfRepositories(state : StatusRepository) -> Int{
        return (repositories?.filter{StatusRepository(rawValue:$0.state!) == state}.count)!
    }

}


